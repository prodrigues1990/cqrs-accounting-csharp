using Domain.Accounts;
using NUnit.Framework;
using System;
using System.Linq;

namespace Domain.Tests
{
    public class TestOpenAccount
    {
        private Account account;
        private Guid accountId = Guid.NewGuid();
        private string accountName = "Test account";

        [SetUp]
        public void Setup()
        {
            var cmd = new OpenAccount(accountId, accountName);
            account = Account.Open(cmd);
        }

        [Test]
        public void Test1()
        {
            Assert.IsNotEmpty(account.Events);
            Assert.AreEqual(account.Events.Last(), account.Events.First());
            Assert.IsInstanceOf(typeof(AccountOpened), account.Events.First());
        }
    }
}