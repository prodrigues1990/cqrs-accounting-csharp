﻿using Domain.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace GUI
{
    interface IView
    {
        void Render(AccountListItemDto account);
        void Render(IEnumerable<AccountListItemDto> accounts);
    }
}
