﻿using Domain.Accounts;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GUI
{
    partial class AccountsListForm : Form, IView
    {
        private readonly Controller controller;
        private readonly TransferFundsDialog transferFundsDialog;
        public AccountsListForm(Controller controller)
        {
            InitializeComponent();
            this.controller = controller;
            transferFundsDialog = new TransferFundsDialog(controller, this);
        }

        public void Render(AccountListItemDto account)
        {
            throw new NotImplementedException();
        }

        public void Render(IEnumerable<AccountListItemDto> accounts)
        {
            listViewAccounts.Items.Clear();
            foreach (var account in accounts)
            {
                var item = new ListViewItem(new string[] { account.Name, account.Balance + "€" });
                listViewAccounts.Items.Add(item);
            }

            transferFundsDialog.Render(accounts);
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "openAccount":
                    new OpenAccountDialog(controller, this).Show();
                    break;
                case "transferFunds":
                    transferFundsDialog.Show();
                    break;
                default: break;
            }
        }

        private void AccountsListForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            transferFundsDialog.Dispose();
        }
    }
}
