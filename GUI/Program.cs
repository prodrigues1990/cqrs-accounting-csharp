using Domain;
using Domain.Accounts;
using System;
using System.Windows.Forms;

namespace GUI
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var bus = new Bus();
            var writeRepository = new FakeRepository<Account>();
            var service = new AccountService(writeRepository, bus);
            var readRepository = new FakeReadModelFacade();
            var controller = new Controller(readRepository, service);

            var accountsListView = new AccountListView(readRepository);
            bus.RegisterHandler<AccountOpened>(accountsListView.Handle);
            bus.RegisterHandler<AccountBalanceChanged>(accountsListView.Handle);

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AccountsListForm(controller));
        }
    }
}
