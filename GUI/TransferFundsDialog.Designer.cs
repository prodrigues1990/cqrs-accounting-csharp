﻿namespace GUI
{
    partial class TransferFundsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxFromAccount = new System.Windows.Forms.ComboBox();
            this.numericUpDownAmount = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxToAccount = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonConfirmar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "From";
            // 
            // comboBoxFromAccount
            // 
            this.comboBoxFromAccount.FormattingEnabled = true;
            this.comboBoxFromAccount.Location = new System.Drawing.Point(91, 12);
            this.comboBoxFromAccount.Name = "comboBoxFromAccount";
            this.comboBoxFromAccount.Size = new System.Drawing.Size(136, 23);
            this.comboBoxFromAccount.TabIndex = 1;
            // 
            // numericUpDownAmount
            // 
            this.numericUpDownAmount.Location = new System.Drawing.Point(144, 70);
            this.numericUpDownAmount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownAmount.Name = "numericUpDownAmount";
            this.numericUpDownAmount.Size = new System.Drawing.Size(83, 23);
            this.numericUpDownAmount.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "To";
            // 
            // comboBoxToAccount
            // 
            this.comboBoxToAccount.FormattingEnabled = true;
            this.comboBoxToAccount.Location = new System.Drawing.Point(91, 41);
            this.comboBoxToAccount.Name = "comboBoxToAccount";
            this.comboBoxToAccount.Size = new System.Drawing.Size(136, 23);
            this.comboBoxToAccount.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Amount";
            // 
            // buttonConfirmar
            // 
            this.buttonConfirmar.Location = new System.Drawing.Point(80, 99);
            this.buttonConfirmar.Name = "buttonConfirmar";
            this.buttonConfirmar.Size = new System.Drawing.Size(75, 23);
            this.buttonConfirmar.TabIndex = 3;
            this.buttonConfirmar.Text = "Confirmar";
            this.buttonConfirmar.UseVisualStyleBackColor = true;
            this.buttonConfirmar.Click += new System.EventHandler(this.buttonConfirmar_Click);
            // 
            // TransferFundsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 130);
            this.Controls.Add(this.buttonConfirmar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxToAccount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDownAmount);
            this.Controls.Add(this.comboBoxFromAccount);
            this.Controls.Add(this.label1);
            this.Name = "TransferFundsDialog";
            this.Text = "Transferir Fundos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TransferFundsDialog_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAmount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxFromAccount;
        private System.Windows.Forms.NumericUpDown numericUpDownAmount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxToAccount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonConfirmar;
    }
}