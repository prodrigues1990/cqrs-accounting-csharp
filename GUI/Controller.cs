﻿using Domain.Accounts;
using System;
using System.Linq;

namespace GUI
{
    class Controller
    {
        private readonly IReadModelFacade repository;
        private readonly AccountService service;

        public Controller(IReadModelFacade repository, AccountService service)
        {
            this.repository = repository;
            this.service = service;
        }

        public void OnLoad(IView view)
        {
            view.Render(repository.GetAccounts());
        }
        public void OnAccountListItemClicked(IView view, Guid id)
        {
            view.Render(repository.GetAccounts().Where(e => e.Id == id).First());
        }
        public void OnCreateAccountClicked(IView view, string name)
        {
            service.Handle(new OpenAccount(Guid.NewGuid(), name));
            view.Render(repository.GetAccounts());
        }
        public void OnTransferFundsClicked(IView view, Guid from, Guid to, decimal amount)
        {
            service.Handle(new TransferFunds(from, to, amount));
            view.Render(repository.GetAccounts());
        }
    }
}
