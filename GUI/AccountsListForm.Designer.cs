﻿namespace GUI
{
    partial class AccountsListForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AccountsListForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.openAccount = new System.Windows.Forms.ToolStripButton();
            this.transferFunds = new System.Windows.Forms.ToolStripButton();
            this.name = new System.Windows.Forms.ColumnHeader();
            this.balance = new System.Windows.Forms.ColumnHeader();
            this.listViewAccounts = new System.Windows.Forms.ListView();
            this.accountName = new System.Windows.Forms.ColumnHeader();
            this.accountBalance = new System.Windows.Forms.ColumnHeader();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openAccount,
            this.transferFunds});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(381, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // openAccount
            // 
            this.openAccount.Image = ((System.Drawing.Image)(resources.GetObject("openAccount.Image")));
            this.openAccount.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openAccount.Name = "openAccount";
            this.openAccount.Size = new System.Drawing.Size(104, 22);
            this.openAccount.Text = "&Open Account";
            // 
            // transferFunds
            // 
            this.transferFunds.Image = ((System.Drawing.Image)(resources.GetObject("transferFunds.Image")));
            this.transferFunds.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.transferFunds.Name = "transferFunds";
            this.transferFunds.Size = new System.Drawing.Size(103, 22);
            this.transferFunds.Text = "&Transfer Funds";
            // 
            // name
            // 
            this.name.Text = "Name";
            this.name.Width = 240;
            // 
            // balance
            // 
            this.balance.Text = "Balance";
            // 
            // listViewAccounts
            // 
            this.listViewAccounts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewAccounts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.accountName,
            this.accountBalance});
            this.listViewAccounts.FullRowSelect = true;
            this.listViewAccounts.HideSelection = false;
            this.listViewAccounts.Location = new System.Drawing.Point(12, 28);
            this.listViewAccounts.Name = "listViewAccounts";
            this.listViewAccounts.Size = new System.Drawing.Size(357, 141);
            this.listViewAccounts.TabIndex = 3;
            this.listViewAccounts.UseCompatibleStateImageBehavior = false;
            this.listViewAccounts.View = System.Windows.Forms.View.Details;
            // 
            // accountName
            // 
            this.accountName.Text = "Name";
            this.accountName.Width = 240;
            // 
            // accountBalance
            // 
            this.accountBalance.Text = "Balance";
            // 
            // AccountsListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 181);
            this.Controls.Add(this.listViewAccounts);
            this.Controls.Add(this.toolStrip1);
            this.Name = "AccountsListForm";
            this.Text = "Accounts";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AccountsListForm_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton openAccount;
        private System.Windows.Forms.ToolStripButton transferFunds;
        private System.Windows.Forms.ColumnHeader name;
        private System.Windows.Forms.ColumnHeader balance;
        private System.Windows.Forms.ListView listViewAccounts;
        private System.Windows.Forms.ColumnHeader accountName;
        private System.Windows.Forms.ColumnHeader accountBalance;
    }
}

