﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GUI
{
    partial class OpenAccountDialog : Form
    {
        private readonly Controller controller;
        private readonly IView view;
        public OpenAccountDialog(Controller controller, IView view)
        {
            InitializeComponent();
            this.controller = controller;
            this.view = view;
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            controller.OnCreateAccountClicked(view, txtName.Text);
            Close();
        }
    }
}
