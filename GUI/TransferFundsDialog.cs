﻿using Domain.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace GUI
{
    partial class TransferFundsDialog : Form
    {
        private readonly Controller controller;
        private readonly IView view;
        private readonly BindingSource fromAccounts = new BindingSource();
        private readonly BindingSource toAccounts = new BindingSource();
        public TransferFundsDialog(Controller controller, IView view)
        {
            InitializeComponent();
            this.controller = controller;
            this.view = view;

            comboBoxFromAccount.DisplayMember = "Name";
            comboBoxFromAccount.ValueMember = "Id";
            comboBoxToAccount.DisplayMember = "Name";
            comboBoxToAccount.ValueMember = "Id";
        }
        public void Render(IEnumerable<AccountListItemDto> accounts)
        {
            fromAccounts.DataSource = accounts.ToList();
            toAccounts.DataSource = accounts.ToList();
            comboBoxFromAccount.DataSource = fromAccounts;
            comboBoxToAccount.DataSource = toAccounts;
        }

        private void buttonConfirmar_Click(object sender, EventArgs e)
        {
            dynamic from = comboBoxFromAccount.SelectedItem;
            dynamic to = comboBoxToAccount.SelectedItem;
            decimal amount = numericUpDownAmount.Value;

            if (from == to || amount <= 0)
                return;

            controller.OnTransferFundsClicked(view, from.Id, to.Id, amount);

            comboBoxFromAccount.SelectedIndex = -1;
            comboBoxToAccount.SelectedIndex = -1;
            numericUpDownAmount.Value = 0;
            Hide();
        }

        private void TransferFundsDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }
    }
}
