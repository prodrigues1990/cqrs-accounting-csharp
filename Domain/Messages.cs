﻿using System;

namespace Domain
{
    public interface Message { }
    public interface Command : Message { }
    public interface Event : Message
    {
        Guid Id { get; }
    }
}
