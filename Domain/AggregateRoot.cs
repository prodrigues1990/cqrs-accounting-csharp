﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public abstract class AggregateRoot
    {
        private readonly List<Event> events = new List<Event>();
        public abstract Guid Id { get; }
        public IEnumerable<Event> Events => events;
        protected void Raise(Event @event)
        {
            events.Add(@event);
        }
        public void ClearEvents()
        {
            events.Clear();
        }
    }
}
