﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Bus : ICommandSender, IEventPublisher
    {
        private readonly Dictionary<Type, List<Action<Message>>> handlers = new Dictionary<Type, List<Action<Message>>>();
        public void RegisterHandler<T>(Action<T> handler) where T : Message
        {
            List<Action<Message>> _handlers;

            if (!handlers.TryGetValue(typeof(T), out _handlers))
            {
                _handlers = new List<Action<Message>>();
                handlers.Add(typeof(T), _handlers);
            }

            _handlers.Add(x => handler((T)x));
        }
        public void Publish<T>(T @event) where T : Event
        {
            List<Action<Message>> _handlers;

            if (!handlers.TryGetValue(@event.GetType(), out _handlers)) return;

            foreach (var handler in _handlers)
                handler(@event);
        }

        public void Send<T>(T command) where T : Command
        {
            List<Action<Message>> _handlers;

            if (handlers.TryGetValue(typeof(T), out _handlers))
            {
                if (_handlers.Count != 1)
                    throw new InvalidOperationException("cannot send to more than one handler");
                _handlers[0](command);
            }
            else
            {
                throw new InvalidOperationException("no handler registered");
            }
        }

        public void Publish<T>(IEnumerable<T> events) where T : Event
        {
            foreach (T @event in events)
                Publish(@event);
        }
    }

    public interface Handles<T>
    {
        void Handle(T message);
    }

    public interface ICommandSender
    {
        void Send<T>(T command) where T : Command;
    }
    public interface IEventPublisher
    {
        void Publish<T>(T @event) where T : Event;
        void Publish<T>(IEnumerable<T> events) where T : Event;
    }
}
