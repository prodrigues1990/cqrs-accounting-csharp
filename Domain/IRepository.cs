﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public interface IRepository<T> where T : AggregateRoot
    {
        void Save(T obj);
        T GetById(Guid id);
    }
    public class FakeRepository<T> : IRepository<T> where T : AggregateRoot
    {
        private readonly Dictionary<Guid, T> db = new Dictionary<Guid, T>();
        public T GetById(Guid id)
        {
            return db[id];
        }

        public void Save(T obj)
        {
            db[obj.Id] = obj;
        }
    }
}
