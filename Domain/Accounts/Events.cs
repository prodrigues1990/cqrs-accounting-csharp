﻿using System;

namespace Domain.Accounts
{
    public class AccountOpened : Event
    {
        public Guid Id { get; }
        public readonly string Name;

        public AccountOpened(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
    }
    public class AccountBalanceChanged : Event
    {
        public Guid Id { get; }
        public readonly decimal Amount;

        public AccountBalanceChanged(Guid id, decimal amount)
        {
            Id = id;
            Amount = amount;
        }
    }
}
