﻿using System;

namespace Domain.Accounts
{
    public class OpenAccount : Command
    {
        public readonly Guid AccountId;
        public readonly string AccountName;

        public OpenAccount(Guid accountId, string accountName)
        {
            AccountId = accountId;
            AccountName = accountName;
        }
    }

    public class TransferFunds : Command
    {
        public readonly Guid FromAccountId;
        public readonly Guid ToAccountId;
        public readonly decimal Amount;

        public TransferFunds(Guid fromAccountId,
                             Guid toAccountId,
                             decimal amount)
        {
            FromAccountId = fromAccountId;
            ToAccountId = toAccountId;
            Amount = amount;
        }
    }
}
