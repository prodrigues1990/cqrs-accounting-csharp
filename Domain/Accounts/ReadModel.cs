﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Domain.Accounts
{
    public interface IReadModelFacade
    {
        IEnumerable<AccountListItemDto> GetAccounts();
    }
    public interface IReadModelDatabase
    {
        AccountListItemDto GetListItem(Guid id);
        void Save(AccountListItemDto item);
    }
    public class FakeReadModelFacade : IReadModelFacade, IReadModelDatabase
    {
        private readonly Dictionary<Guid, AccountListItemDto> db = new Dictionary<Guid, AccountListItemDto>();

        public IEnumerable<AccountListItemDto> GetAccounts()
        {
            return db.Values;
        }

        public AccountListItemDto GetListItem(Guid id)
        {
            return db[id];
        }

        public void Save(AccountListItemDto item)
        {
            db[item.Id] = item;
        }
    }

    public class AccountListItemDto
    {
        public Guid Id { get; }
        public string Name { get; }
        public decimal Balance { get; set; }

        public AccountListItemDto(Guid id, string name, decimal balance)
        {
            Id = id;
            Name = name;
            Balance = balance;
        }
    }
    public class AccountListView : Handles<AccountOpened>, Handles<AccountBalanceChanged>
    {
        private readonly IReadModelDatabase db;

        public AccountListView(IReadModelDatabase db)
        {
            this.db = db;
        }

        public void Handle(AccountOpened message)
        {
            var item = new AccountListItemDto(message.Id, message.Name, 0);
            db.Save(item);
        }

        public void Handle(AccountBalanceChanged message)
        {
            var item = db.GetListItem(message.Id);
            item.Balance += message.Amount;
            db.Save(item);
        }
    }
}
