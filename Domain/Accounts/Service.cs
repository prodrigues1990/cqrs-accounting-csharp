﻿namespace Domain.Accounts
{
    public class AccountService
    {
        private readonly IRepository<Account> repo;
        private readonly IEventPublisher bus;

        public AccountService(IRepository<Account> repo, IEventPublisher bus)
        {
            this.repo = repo;
            this.bus = bus;
        }
        public void Handle(OpenAccount command)
        {
            var account = Account.Open(command);
            repo.Save(account);
            bus.Publish(account.Events);
        }
        public void Handle(TransferFunds command)
        {
            var fromAcc = repo.GetById(command.FromAccountId);
            var toAcc = repo.GetById(command.ToAccountId);
            fromAcc.Debit(command.Amount);
            toAcc.Credit(command.Amount);
            repo.Save(fromAcc);
            repo.Save(toAcc);
            bus.Publish(fromAcc.Events);
            bus.Publish(toAcc.Events);
        }
    }
}
