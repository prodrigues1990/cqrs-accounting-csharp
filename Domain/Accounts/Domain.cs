﻿using System;

namespace Domain.Accounts
{
    public class Account : AggregateRoot
    {
        public override Guid Id { get; }
        public readonly string Name;
        public Account(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
        public static Account Open(OpenAccount command)
        {
            var obj = new Account(command.AccountId, command.AccountName);
            obj.Raise(new AccountOpened(obj.Id, obj.Name));
            return obj;
        }
        public void Credit(decimal amount)
        {
            Raise(new AccountBalanceChanged(Id, amount));
        }
        public void Debit(decimal amount)
        {
            Raise(new AccountBalanceChanged(Id, -amount));
        }
    }
}
